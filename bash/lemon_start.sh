#!/bin/bash

#CONSTANT VARIABLES
BAR_GEOMETRY="1356x25+5+2"
BG_COLOR="#2e3436"


conky | lemonbar \
-p \
-g $BAR_GEOMETRY \
-B $BG_COLOR \
-f '8x13bold' \
| $SHELL
